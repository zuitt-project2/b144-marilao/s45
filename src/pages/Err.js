// import {Fragment} from 'react';
import Banner from '../components/Banner';
// import Home from '../pages/Home';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}

/*const Err = () => {
	
		 return (
	        <div id="wrapper">
	            <img src="https://i.imgur.com/qIufhof.png" />
	            <div id="info">
	                <h1>Page Not Found</h1>
	                <h3>Go back to the <a href='/'>homepage</a>.</h3>
	            </div>
	        </div >
	    )
}

export default Err;*/