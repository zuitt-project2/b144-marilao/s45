import {useState, useEffect, useContext} from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const { user, optionsState } = useContext(UserContext);

	const history = useHistory();
	

	// State hooks to store the values of the input fields
	const [firstName, setFname] = useState('');
	const [lastName, setLname] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	/*const [checkEmail, setCheckEmail ] = useState(false)*/

	// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(age);
	console.log(gender);
	console.log(mobileNo);
	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		// Prevents page redirection viaform submission
		e.preventDefault();


			fetch('http://localhost:4000/api/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				
				if(data === true) {
					Swal.fire({
					title: 'Duplicate Email found!',
					icon: 'error',
					text: 'Please provide a different email.'
				})
				
			}
			else{
				fetch('http://localhost:4000/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						age: age,
						gender: gender,
						email: email,
						mobileNo: mobileNo,
						password: password1
						
					})
					
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
	
					if(data === true){
						Swal.fire({
							title: "Successfully registered",
							icon: "success",
							text: "You have successfully registered an account."
						})
						setFname('');
						setLname('');
						setAge('');
						setGender('');
						setEmail('');
						setMobileNo('');
						setPassword1('');
						setPassword2('');
						history.push("/login")
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		// Validation to enable the submit button when all fields are populated and both passwords match
		if(( firstName !== '' && lastName !== '' && age !== '' && mobileNo !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, age, gender, mobileNo, email, password1, password2])

return (
	// activity : If the user goes to /register route (Registration page) when a user is already logged in, redirect the user to the /courses route (Courses page).
	(user.id !== null) ?
			<Redirect to='/courses'/>
		:
	<Form onSubmit={(e) => registerUser(e)}>
	<h1>Register</h1>
 		<Form.Group className="mb-3" controlId="userEmail">
 			<Form.Label>First Name</Form.Label>
    		<Form.Control 
    			type="fname" 
    			placeholder="Enter first name" 
    			value= {firstName}
    			onChange={ e => setFname(e.target.value)}
    			required 
    		/> 
   		 	<Form.Label>Last Name</Form.Label>
    		<Form.Control 
    			type="lname" 
    			placeholder="Enter last name" 
    			value= {lastName}
    			onChange={ e => setLname(e.target.value)}
    			required 
    		/> 
    		<Form.Label>Age</Form.Label>
    		<Form.Control 
    			type="age" 
    			placeholder="Enter Age" 
    			value= {age}
    			onChange={ e => setAge(e.target.value)}
    			required 
    		/><br/> 

    	<Form.Group className="mb-3" controlId="gender">
    		<Form.Label>Gender</Form.Label><br/>
  			<select value={optionsState}>
 				 <option value="male">Male</option>
				 <option value="female">Female</option>			  
			</select>
    	</Form.Group>	

    		<Form.Label>Email address</Form.Label>
    		<Form.Control 
    			type="email" 
    			placeholder="Enter email" 
    			value= {email}
    			onChange={ e => setEmail(e.target.value)}
    			required 
    		/> 
   		 	<Form.Text className="text-muted">
      			We'll never share your email with anyone else.
   		 	</Form.Text><br/>

   		 	<Form.Label>Mobile Number</Form.Label>
    		<Form.Control 
    			type="tel" 
    			placeholder="09123456789" 
    			value= {mobileNo}
    			onChange={ e => setMobileNo(e.target.value)}
    			required 
    		/> 
 	 	</Form.Group>

 		 <Form.Group className="mb-3" controlId="password1">
   		 	<Form.Label>Password</Form.Label>
    		<Form.Control 
    			type="password" 
    			placeholder="Password" 
    			value= {password1}
    			onChange={ e => setPassword1(e.target.value)}
    			required 
    		/>
  		</Form.Group>

  	 	<Form.Group className="mb-3" controlId="password2">
   		 	<Form.Label>Verify Password</Form.Label>
    		<Form.Control 
    			type="password" 
    			placeholder="Verify Password" 
    			value= {password2}
    			onChange={ e => setPassword2(e.target.value)}
    			required 
    		/>
  		</Form.Group>
  		{/* Conditionally render submit button based on isActive state.*/}

  		{ isActive ?
	 	 	<Button variant="primary" type="submit" id = 'submitBtn'>
	    		Submit
	 		 </Button>
	 	:
	 		<Button variant="warning" type="submit" id = 'submitBtn' disabled>
	    		Submit
	 		 </Button>
  		}
	</Form>
	)
}