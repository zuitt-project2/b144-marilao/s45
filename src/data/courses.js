// Mock Database
const courseData = [
	{
		id : 'wdc001',
		name : 'PHP - Laravel',
		description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque faucibus, sapien vel varius dapibus, dui erat egestas eros, quis tincidunt sem metus sit amet dolor. Nulla ac vulputate eros, ut placerat nunc. Pellentesque porta rhoncus quam ut auctor.',
		price : 45000,
		onOffer : true
	},
	{
		id : 'wdc002',
		name : 'Python - Django',
		description : 'Maecenas tristique, nisl quis ultricies sollicitudin, neque ligula tempor dui, vel accumsan elit est at erat. Integer feugiat, tellus vel laoreet efficitur, sapien diam congue odio, in egestas elit nisl in augue.',
		price : 50000,
		onOffer : true
	},
	{
		id : 'wdc003',
		name : 'Java - Springboot',
		description : 'Nullam gravida congue ante, non scelerisque est tristique non. Fusce vitae felis sed magna rutrum pharetra vitae vel ipsum. Vivamus ipsum quam, lobortis eu imperdiet nec, varius condimentum felis.',
		price : 55000,
		onOffer : true
	},
	{
		id : 'wdc004',
		name : 'HTML Introduction',
		description : 'Nullam gravida congue ante, non scelerisque est tristique non. Fusce vitae felis sed magna rutrum pharetra vitae vel ipsum. Vivamus ipsum quam, lobortis eu imperdiet nec, varius condimentum felis.',
		price : 60000,
		onOffer : true
	}
];

export default courseData;