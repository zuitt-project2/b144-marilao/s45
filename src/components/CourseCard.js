// import state hook from react
// import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed
	/*console.log(props);
	console.log(typeof props);*/
	const {_id, name, description, price} = courseProp;

	// Use state hook in this component to be able to store its state
	// States are used to keep track of info related to individual components
	/* Syntax :
		const [getter, setter] = useState(initialGetterValue);
	*/
	// const [count, setCount] = useState(0);
	// const [deduct, setDeduct] = useState(30);
	// const [isOpen, setIsOpen]= useState(false);

	// console.log(useState(0));

	// Function to keep track of the enrollees for a course

	// Activity: 1. Create a seats state in the CourseCard component and set the initial value to 30. 2. For every enrollment, deduct one to the seats. 3. If the seats reaches zero do the following:
		/*- Do not add to the count.
		- Do not deduct to the seats.*/
	// function enroll() {
		/*if(count !== 30){*/
			/*setCount(count + 1);
			console.log('Enrollees: ' + count);
			setDeduct(deduct - 1);
			console.log('Seats: ' + deduct);*/
		/*}else if(deduct === 0){
			alert `No more seats available!`;	
		}*/
	// }

	// Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update
	/*useEffect(() => {
		if(deduct === 0){
			setIsOpen(true);
		}
	}, [deduct])*/
	
    return (
        <Card className= 'mb-2'>
           <Card.Body >
               <Card.Title>{name}</Card.Title>
               <Card.Subtitle>Description:</Card.Subtitle>
               <Card.Text>{description}</Card.Text>
               <Card.Subtitle>Price:</Card.Subtitle>
               <Card.Text>PhP {price} </Card.Text>
               <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
           </Card.Body>
       </Card> 
    )
}
	
